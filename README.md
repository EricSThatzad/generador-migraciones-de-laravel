## Generador de migraciones de Laravel


<p>Proyecto de laravel con la unica funcionalidad de generar migraciones desde una base de datos ya existente. El proyecto utiliza la librería <a href="ttps://github.com/kitloong/laravel-migrations-generator" target"_blank">kitloong/laravel-migrations-generator</a> para realizar esta tareas</p>

## Versiones
- PHP: 8.0 <br>
- LARAVEL: 9.0

## Pasos a seguir
<p>Establecer .env, asociar la base de datos, lanzar el comando "composer install" y por ulrimo lanzar el comando "php artisan migrate:generate".